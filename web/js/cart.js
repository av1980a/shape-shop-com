function showCart(cart) {
	$('#cart .modal-body').html(cart);
	$('#cart').modal();
}

function clearCart() {
	$.ajax({
		url: '/cart/clear',		
		type: 'GET',
		success: function (res) {
			if (!res) alert ('Error!');
 			//console.log (res);
			showCart (res);
			//$('#cart .modal-body').html(cart);
			//$('#cart').modal();
		},
		error: function () {
			alert ('Error!');
		}
	});
}

function getCart() {
	$.ajax({
		url: '/cart/show',		
		type: 'GET',
		success: function (res) {
			if (!res) alert ('Корзина пуста!');
 			//console.log (res);
			showCart (res);
			//$('#cart .modal-body').html(cart);
			//$('#cart').modal();
		},
		error: function () {
			alert ('Ошибка соединения!');
		}
	});
}

$('#cart .modal-body').on('click', '.del-item', function delItem() {
	var id = $(this).data('id');
	$.ajax({
		url: '/cart/del-item',
		data: {id: id},
		type: 'GET',
		success: function (res) {
			if (!res) alert ('Error!');
 			//console.log (res);
			showCart (res);
			//$('#cart .modal-body').html(cart);
			//$('#cart').modal();
		},
		error: function () {
			alert ('Error!');
		}
	});
})

$(".item_add").on('click', function addCart() {
	//e.preventDefault();
	//e.stopPropagation();
	event.preventDefault()
	var id = $(this).data('id'),
	    qty = $('input:text').val();
	$.ajax({
		url: '/cart/add',
		data: {id: id, qty: qty},
		type: 'GET',
		success: function (res) {
			if (!res) alert ('Error!');
 			//console.log (res);
			showCart (res);
			//$('#cart .modal-body').html(cart);
			//$('#cart').modal();
		},
		error: function () {
			alert ('Error!');
		}
	});
})