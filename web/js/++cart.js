function showCart(cart) {
	$('#cart .modal-body').html(cart);
	$('#cart').modal();
}

$(".item_add").on("click", function (e) {
	e.preventDefault();
	var id = $(this).data('id');
	$.ajax({
		url: '/cart/add',
		data: {id: id},
		type: 'GET',
		success: function (res) {
			if (!res) alert ('Error!');
 			//console.log (res);
			showCart (res);
			//$('#cart .modal-body').html(cart);
			//$('#cart').modal();
		},
		error: function () {
			alert ('Error!');
		}
	});
});