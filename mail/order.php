<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Наименование</th>
				<th>Кол-во</th>
				<th>Цена</th>				
			</tr>
		</thead>
		<tbody>
<?php foreach ($session['cart'] as $id => $item):?>
            <tr>
				<td><?php  echo $item['name']?></td>
				<td><?php  echo $item['qty']?></td>
				<td><?php  echo $item['price']?></td>				
			</tr>
<?php endforeach;?>
<tr>
				<td colspan="2">Итого:</td>
				<td><?php echo $session['cart.qty']?>
</td>
<tr>
				<td colspan="2">Сумма:</td>
				<td><?php echo $session['cart.sum']?>
</td>
		
		</tbody>
	</table>
</div>

