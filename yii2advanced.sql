-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 13, 2018 at 09:45 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2advanced`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `keywords`, `description`) VALUES
(1, 0, 'Тест категории 1', 'Тестовые ключевики категории 1', 'Тестовое описание категории 1-123123123'),
(2, 1, 'Тест категории 2', 'Тестовые ключевики категории 2', 'Тестовое описание категории 2'),
(3, 1, 'Тест категории 3', 'Тестовые ключевики категории 3', 'Тестовое описание категории 3'),
(4, 1, 'Тест категории 4', 'Тестовые ключевики категории 4', 'Тестовое описание категории 4'),
(5, 1, 'Тест категории 5', 'Тестовые ключевики категории 5', 'Тестовое описание категории 5'),
(6, 0, 'Тест категории 6', 'Тестовые ключевики категории 6', 'Тестовое описание категории 6'),
(7, 6, 'Тест категории 7', 'Тестовые ключевики категории 7', 'Тестовое описание категории 7'),
(8, 6, 'Тест категории 8', 'Тестовые ключевики категории 8', 'Тестовое описание категории 8'),
(9, 6, 'Тест категории 9', 'Тестовые ключевики категории 9', 'Тестовое описание категории 9'),
(10, 6, 'Тест категории 10', 'Тестовые ключевики категории 10', 'Тестовое описание категории 10'),
(11, 0, 'Тест категории 11', 'Тестовые ключевики категории 11', 'Тестовое описание категории 11'),
(12, 11, 'Тест категории 12', 'Тестовые ключевики категории 12', 'Тестовое описание категории 12'),
(13, 11, 'Тест категории 13', 'Тестовые ключевики категории 13', 'Тестовое описание категории 13'),
(14, 11, 'Тест категории 14', 'Тестовые ключевики категории 14', 'Тестовое описание категории 14'),
(15, 11, 'Тест категории 15', 'Тестовые ключевики категории 15', 'Тестовое описание категории 15');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `isMain` tinyint(1) DEFAULT NULL,
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL,
  `name` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `filePath`, `itemId`, `isMain`, `modelName`, `urlAlias`, `name`) VALUES
(1, 'Products/Product9/4e899e.png', 9, 0, 'Product', '67f5f2ef37-3', ''),
(2, 'Products/Product9/6e6e7b.png', 9, 0, 'Product', 'baddf1a949-4', ''),
(3, 'Products/Product9/405ea5.png', 9, 0, 'Product', '4e36f562a9-5', ''),
(4, 'Products/Product9/d86312.png', 9, 0, 'Product', 'ad9d1400a5-6', ''),
(5, 'Products/Product9/2c31da.png', 9, 0, 'Product', '786b83ff4f-2', ''),
(6, 'Products/Product12/803e4f.png', 12, 0, 'Product', 'fb1b2f8ca5-3', ''),
(7, 'Products/Product12/43b3e8.png', 12, 0, 'Product', '33011146a5-2', ''),
(8, 'Products/Product12/8156fd.png', 12, 1, 'Product', 'b056fa748f-1', ''),
(9, 'Products/Product12/901aec.png', 12, NULL, 'Product', '78e83c5653-4', ''),
(10, 'Products/Product9/2c1cd2.png', 9, 1, 'Product', '422f2e2730-1', ''),
(11, 'Products/Product9/250b0c.png', 9, NULL, 'Product', '777988b8bb-7', ''),
(12, 'Products/Product9/4618ce.png', 9, NULL, 'Product', 'acc2004ee2-8', '');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1518886746),
('m130524_201442_init', 1518886760),
('m140622_111540_create_image_table', 1526054077),
('m140622_111545_add_name_to_image_table', 1526054077);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `qty` int(10) NOT NULL,
  `sum` float NOT NULL,
  `status` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `created_at`, `updated_at`, `qty`, `sum`, `status`, `name`, `email`, `phone`, `address`) VALUES
(1, '2018-04-04 20:12:38', '2018-04-04 20:12:38', 2, 3700, '1', '45345', '4534545345', '45456453453453453', '453453453453453453453453'),
(2, '2018-04-04 20:18:28', '2018-04-04 20:18:28', 2, 3700, '0', '5745', '453443453', '454534', '4534534'),
(3, '2018-04-04 20:31:48', '2018-04-04 20:31:48', 1, 1900, '0', '543453', '454555', '4565456', '76645467864'),
(4, '2018-04-04 20:34:14', '2018-04-04 20:34:14', 1, 1900, '0', '543453', '454555', '4565456', '76645467864'),
(5, '2018-04-04 20:34:27', '2018-04-04 20:34:27', 1, 1900, '0', '543453', '454555', '4565456', '76645467864'),
(6, '2018-04-04 20:37:26', '2018-04-04 20:37:26', 1, 1900, '0', '543453', '454555', '4565456', '76645467864'),
(7, '2018-04-04 20:40:13', '2018-04-04 20:40:13', 1, 1900, '0', '543453', '454555', '4565456', '76645467864'),
(8, '2018-04-04 20:40:38', '2018-04-04 20:40:38', 1, 1900, '0', '543453', '454555', '4565456', '76645467864'),
(9, '2018-05-03 13:40:36', '2018-05-03 13:40:36', 2, 3000, '0', '45345', '453443453', '45456453453453453', '4534534'),
(10, '2018-05-03 13:41:55', '2018-05-03 13:41:55', 2, 3000, '0', '543453', '4534545345', '4565456', '453453453453453453453453'),
(11, '2018-05-03 13:42:52', '2018-05-03 13:42:52', 2, 3000, '0', '45345', '454555@mail.ru', '45456453453453453', '4534534'),
(12, '2018-05-03 13:43:30', '2018-05-03 13:43:30', 2, 3000, '0', '5745', 'gyjddyu@mail.ru', '454534', '453453453453453453453453'),
(13, '2018-05-03 13:53:16', '2018-05-03 13:53:16', 2, 3000, '0', '543453', 'gyjddyu@mail.ru', '45456453453453453', '76645467864'),
(14, '2018-05-03 13:54:25', '2018-05-03 13:54:25', 2, 3000, '0', '45345', 'gyjddyu@mail.ru', '45456453453453453', '453453453453453453453453'),
(15, '2018-05-03 13:56:47', '2018-05-03 13:56:47', 2, 3000, '0', '45345', 'gyjddyu@mail.ru', '454534', '453453453453453453453453'),
(16, '2018-05-06 22:58:07', '2018-05-06 22:58:07', 2, 3700, '0', '45345', 'gyjddyu@mail.ru', '45456453453453453', '4534534олд');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `qty_item` int(11) NOT NULL,
  `sum_item` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `name`, `price`, `qty_item`, `sum_item`) VALUES
(1, 8, 4, 'Тест товара 4', 1900, 1, 1900),
(2, 9, 2, 'Тест товара 2', 1500, 2, 3000),
(3, 10, 2, 'Тест товара 2', 1500, 2, 3000),
(4, 11, 2, 'Тест товара 2', 1500, 2, 3000),
(5, 12, 2, 'Тест товара 2', 1500, 2, 3000),
(6, 13, 2, 'Тест товара 2', 1500, 2, 3000),
(7, 14, 2, 'Тест товара 2', 1500, 2, 3000),
(8, 15, 2, 'Тест товара 2', 1500, 2, 3000),
(9, 16, 4, 'Тест товара 4', 1900, 1, 1900),
(10, 16, 3, 'Тест товара 3', 1800, 1, 1800);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8,
  `price` float NOT NULL DEFAULT '0',
  `keywords` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'no-image.png',
  `sale` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `new` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `hit` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `content`, `price`, `keywords`, `description`, `img`, `sale`, `new`, `hit`) VALUES
(1, 2, 'Тест товара 1', 'Тест расширенного описания товара 1', 1200, 'Тест ключевики товара 1', 'Тест краткого описания товара 1', 'no-image.png', '1', '0', '0'),
(2, 2, 'Тест товара 2', 'Тест расширенного описания товара 2', 1500, 'Тест ключевики товара 2', 'Тест краткого описания товара 2', 'no-image.png', '0', '1', '0'),
(3, 3, 'Тест товара 3', 'Тест расширенного описания товара 3', 1800, 'Тест ключевики товара 3', 'Тест краткого описания товара 3', 'no-image.png', '0', '0', '0'),
(4, 3, 'Тест товара 4', 'Тест расширенного описания товара 4', 1900, 'Тест ключевики товара 4', 'Тест краткого описания товара 4', 'no-image.png', '1', '0', '0'),
(5, 4, 'Тест товара 5', 'Тест расширенного описания товара 5', 2000, 'Тест ключевики товара 5', 'Тест краткого описания товара 5', 'no-image.png', '0', '0', '0'),
(6, 4, 'Тест товара 6', 'Тест расширенного описания товара 6', 2000, 'Тест ключевики товара 6', 'Тест краткого описания товара 6', 'no-image.png', '1', '0', '0'),
(7, 5, 'Тест товара 7', 'Тест расширенного описания товара 7', 2100, 'Тест ключевики товара 7', 'Тест краткого описания товара 7', 'no-image.png', '0', '0', '0'),
(8, 5, 'Тест товара 8', 'Тест расширенного описания товара 8', 2200, 'Тест ключевики товара 8', 'Тест краткого описания товара 8', 'no-image.png', '1', '0', '0'),
(9, 7, 'Тест товара 11', '<p>Тест расширенного описания товара 11</p>\r\n', 1200, 'Тест ключевики товара 11', 'Тест краткого описания товара 11', 'no-image.png', '1', '0', '0'),
(10, 7, 'Тест товара 12', 'Тест расширенного описания товара 12', 1500, 'Тест ключевики товара 12', 'Тест краткого описания товара 12', 'no-image.png', '0', '0', '1'),
(11, 8, 'Тест товара 13', 'Тест расширенного описания товара 13', 1800, 'Тест ключевики товара 13', 'Тест краткого описания товара 13', 'no-image.png', '0', '0', '0'),
(12, 8, 'Тест товара 14', '<p>Тест расширенного описания товара 14</p>\r\n', 1900, 'Тест ключевики товара 14', 'Тест краткого описания товара 14', 'no-image.png', '1', '0', '0'),
(13, 8, 'Тест товара 15', 'Тест расширенного описания товара 15', 2000, 'Тест ключевики товара 15', 'Тест краткого описания товара 15', 'no-image.png', '0', '0', '0'),
(14, 9, 'Тест товара 16', 'Тест расширенного описания товара 16', 2000, 'Тест ключевики товара 16', 'Тест краткого описания товара 16', 'no-image.png', '1', '0', '0'),
(15, 9, 'Тест товара 17', 'Тест расширенного описания товара 17', 2100, 'Тест ключевики товара 17', 'Тест краткого описания товара 17', 'no-image.png', '0', '0', '0'),
(16, 9, 'Тест товара 18', 'Тест расширенного описания товара 18', 2200, 'Тест ключевики товара 18', 'Тест краткого описания товара 18', 'no-image.png', '1', '0', '0'),
(17, 12, 'Тест товара 111', 'Тест расширенного описания товара 111', 1200, 'Тест ключевики товара 111', 'Тест краткого описания товара 111', 'no-image.png', '1', '0', '0'),
(18, 12, 'Тест товара 12', '<p>Тест расширенного описания товара 112</p>\r\n', 1500, 'Тест ключевики товара 112', 'Тест краткого описания товара 112', 'no-image.png', '0', '0', '0'),
(19, 13, 'Тест товара 113', 'Тест расширенного описания товара 113', 1800, 'Тест ключевики товара 113', 'Тест краткого описания товара 113', 'no-image.png', '0', '0', '0'),
(20, 13, 'Тест товара 114', 'Тест расширенного описания товара 114', 1900, 'Тест ключевики товара 114', 'Тест краткого описания товара 114', 'no-image.png', '1', '0', '0'),
(21, 14, 'Тест товара 115', 'Тест расширенного описания товара 115', 2000, 'Тест ключевики товара 115', 'Тест краткого описания товара 115', 'no-image.png', '0', '0', '0'),
(22, 14, 'Тест товара 116', 'Тест расширенного описания товара 116', 2000, 'Тест ключевики товара 116', 'Тест краткого описания товара 116', 'no-image.png', '1', '0', '0'),
(23, 15, 'Тест товара 117', 'Тест расширенного описания товара 117', 2100, 'Тест ключевики товара 117', 'Тест краткого описания товара 117', 'no-image.png', '0', '0', '0'),
(24, 15, 'Тест товара 118', 'Тест расширенного описания товара 118', 2200, 'Тест ключевики товара 118', 'Тест краткого описания товара 118', 'no-image.png', '1', '0', '0'),
(25, 5, 'Тест товара 119', 'Тест расширенного описания товара 119', 2100, 'Тест ключевики товара 119', 'Тест краткого описания товара 119', 'no-image.png', '0', '0', '0'),
(26, 5, 'Тест товара 120', 'Тест расширенного описания товара 120', 2200, 'Тест ключевики товара 120', 'Тест краткого описания товара 120', 'no-image.png', '1', '0', '0'),
(27, 5, 'Тест товара 121', 'Тест расширенного описания товара 121', 2100, 'Тест ключевики товара 121', 'Тест краткого описания товара 121', 'no-image.png', '0', '0', '0'),
(28, 5, 'Тест товара 122', 'Тест расширенного описания товара 122', 2200, 'Тест ключевики товара 122', 'Тест краткого описания товара 122', 'no-image.png', '1', '0', '0'),
(29, 5, 'Тест товара 123', 'Тест расширенного описания товара 123', 2100, 'Тест ключевики товара 123', 'Тест краткого описания товара 123', 'no-image.png', '0', '0', '0'),
(30, 5, 'Тест товара 124', 'Тест расширенного описания товара 124', 2200, 'Тест ключевики товара 124', 'Тест краткого описания товара 124', 'no-image.png', '1', '0', '0'),
(31, 5, 'Тест товара 125', 'Тест расширенного описания товара 125', 2100, 'Тест ключевики товара 125', 'Тест краткого описания товара 125', 'no-image.png', '0', '0', '0'),
(32, 5, 'Тест товара 126', 'Тест расширенного описания товара 126', 2200, 'Тест ключевики товара 126', 'Тест краткого описания товара 126', 'no-image.png', '1', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`) VALUES
(1, 'admin', '$2y$13$ZfrbDHpBOfFMhhyEXd6N8.rKmUa7AtZPnMz/Gf7spiZNpYP1z3ZXu', 'SDrS7Lni6lJmOVXjzAMBhsFfn8dGFvkj');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
