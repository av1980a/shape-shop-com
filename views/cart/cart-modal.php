<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if (!empty($session['cart'])): ?>
<div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Фото</th>
				<th>Наименование</th>
				<th>Кол-во</th>
				<th>Цена</th>				
			</tr>
		</thead>
		<tbody>
<?php foreach ($session['cart'] as $id => $item):?>
            <tr>
				<td><?php  echo Html::img("@web/images/{$item['img']}", ['alt' => $item['name'], 'height' => 50])?></td>
				<td><?php  echo $item['name']?></td>
				<td><?php  echo $item['qty']?></td>
				<td><?php  echo $item['price']?></td>				
			</tr>
<?php endforeach;?>
<tr>
				<td colspan="3">Итого:</td>
				<td><?php echo $session['cart.qty']?>
</td>
<tr>
				<td colspan="3">Сумма:</td>
				<td><?php echo $session['cart.sum']?>
</td>
		
		</tbody>
	</table>
</div>
<?php else :?>
<h3>Корзина пуста.</h3>
<?php endif;?>
