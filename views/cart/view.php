<?php
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\db\ActiveRecord; 
use yii\widgets\ActiveForm;
?>

<div class="container">
<?php if (Yii::$app->session->hasFlash('success')):?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
<?php echo Yii::$app->session->getFlash('success');?>
</div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('error')):?>
<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
<?php echo Yii::$app->session->getFlash('error');?>
</div>
<?php endif; ?>
<?php if (!empty($session['cart'])): ?>
<div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Фото</th>
				<th>Наименование</th>
				<th>Кол-во</th>
				<th>Цена</th>
				<th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
			</tr>
		</thead>
		<tbody>
<?php foreach ($session['cart'] as $id => $item):?>
            <tr>
				<td><?php  echo Html::img("@web/images/{$item['img']}", ['alt' => $item['name'], 'height' => 50])?></td>
				<td><a href="<?php echo Url::to(['product/view', 'id' => $id])?>"><?php  echo $item['name']?></a></td>
				<td><?php  echo $item['qty']?></td>
				<td><?php  echo $item['price']?></td>
				<td><div><a class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true" href="<?php echo Url::to(['cart/delitem', 'id' => $id])?>"><span data-id="<?php echo $id?>"></span></a></div></td>				
				<!-- <td><div><a class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true" href="" onclick="return delItem()"><span data-id="<?php echo $id?>"></span></a></div></td> -->
			</tr>
<?php endforeach;?>
<tr>
				<td colspan="4">Итого:</td>
				<td><?php echo $session['cart.qty']?>
</td>
<tr>
				<td colspan="4">Сумма:</td>
				<td><?php echo $session['cart.sum']?>
</td>
		
		</tbody>
	</table>
</div>
<hr/>
<?php $form = ActiveForm::begin()?>
<?php echo $form->field($order, 'name');?>
<?php echo $form->field($order, 'email');?>
<?php echo $form->field($order, 'phone');?>
<?php echo $form->field($order, 'address');?>
<?php echo Html::submitButton('Заказать', ['class' => 'btn btn-success'])?>
<?php ActiveForm::end()?>
<?php else :?>
<h3>Корзина пуста.</h3>
<?php endif;?>
</div>