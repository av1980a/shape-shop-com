<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

//$this->title = 'Shape Shop - одежда для активных';
?>

<script>$(document).ready(function(){$(".memenu").memenu();});</script>	

	<?php //echo var_dump($product->category); ?>
	<?php //echo print_r($product); ?>
	
	
	<?php 
	//При переводе сайта на подгрузку картинок товара через админку (rico\yii2images) - раскомментить строки.
	//$mainImg = $product->getImage();
	//$gallery = $product->getImages();
	//$count = count($gallery);
	//$i = 0;
	?>

	<?php 
	//При переводе сайта на подгрузку картинок товара через админку (rico\yii2images) - все ссылки на картинки переписать следующим образом:
	//echo Html::img($mainImg->getUrl(), ['alt' => $product->name]) - для главной картинки
	//*****
	?>
	
	<?php 
	//Для галереи следующие 10 строк:
	//foreach ($gallery as $img):
	//if($i % 4 == 0): 
	?>
	<div class = "item <?php //if ($i == 0) echo 'active'?>">
	 <?php //endif;?>
	 <?php // Здесь вывод src через echo Html::img($mainImg->getUrl(), ['alt' => $product->name]).?>
	 <?php //if ($i % 4 == 0): ?>	 
	 <?php //endif;?>
	 <?php //endforeach;?>
	   </div>
	

<!--start-breadcrumbs-->
<?php if( count($product) > 0): ?>
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="<?php echo Url::home();?>">Главная</a></li>
					<li class="active"><a href="<?php echo Url::to(['category/view', 'id' => $product->category->id])?>"> <?php echo $product->category->name;?></a></li>
				</ol>
			</div>
		</div>
	</div>
	<?php endif;?>
	<!--end-breadcrumbs-->
	<!--start-single-->
	<?php if( count($product) > 0): ?>
	<div class="single contact">
		<div class="container">
			<div class="single-main">
				<div class="col-md-9 single-main-left">
				<div class="sngl-top">
					<div class="col-md-5 single-top-left">	
						<div class="flexslider">
							<ul class="slides">								
								<li data-thumb="<?php echo "/images/s1-" . "{$product->img}" ; ?>">
									<?= Html::img("@web/images/s1-{$product->img}", ['alt' => $product->name]) ?>
								</li>
								<li data-thumb="<?php echo "/images/s2-" . "{$product->img}" ; ?>">
									<?= Html::img("@web/images/s2-{$product->img}", ['alt' => $product->name]) ?>
								</li>
								<li data-thumb="<?php echo "/images/s3-" . "{$product->img}" ; ?>">
									<?= Html::img("@web/images/s3-{$product->img}", ['alt' => $product->name]) ?>
								</li>
								<li data-thumb="<?php echo "/images/s4-" . "{$product->img}" ; ?>">
									<?= Html::img("@web/images/s4-{$product->img}", ['alt' => $product->name]) ?>
								</li>
							</ul>
						</div>
<!-- FlexSlider --> 

	<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});
</script>
				</div>	
				
				<div class="col-md-7 single-top-right">
					<div class="details-left-info simpleCart_shelfItem">
					<?php if( count($product) > 0): ?>
						<h3><?php echo $product->name;?></h3>
						<p class="availability"><a href="<?php echo Url::to(['category/view', 'id' => $product->category->id])?>"> <?php echo "Категория:  " . $product->category->name;?></a></p>
						<p class="availability"><?php echo $product->description;?></p>
						<div class="price_single">
							<span class="reducedfrom"><?php echo $product->price*2 ?>руб</span>
							<span class="actual item_price"><?php echo $product->price ?>руб</span><!-- <a href="#">Добавить в корзину</a> -->
						</div>
						<h2 class="quick">Характеристики:</h2>
						<p class="quick_desc"><?php echo $product->content;?></p>
						<div class="quantity_box">
						<!-- <span>Количество:</span>
						<input type="text" value="5" id="qty" size="5" name="qty"/> -->
							<!-- <ul class="product-qty" id="qty">
								<span>Количество:</span>
								<select>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
								</select>
							</ul> -->
						</div>
					<div class="clearfix"> </div>
				<div class="b-btn">
				         <a class="item_add" href="<?php echo Url::to(['cart/add', 'id' => $product->id]);?>" data-id="<?php echo $product->id;?>">В корзину</a>
				</div>
						<?php else:?>
	                       <h7>Товар отсутствуeт.</h7>
	<?php endif;?>
			</div>
		</div>

		<div class="clearfix"></div>
	</div>
	<?php if( !empty('$sale') ): ?>
					<div class="latest products">
					<h7>Товары месяца</h7>
						<div class="product-one">
						<?php foreach ($sale as $sale1): ?>
							<div class="col-md-4 product-left single-left"> 
								<div class="p-one simpleCart_shelfItem">
									
									<a href="<?php echo Url::to(['product/view', 'id' => $sale1->id])?>">
								<!-- <img src="/images/shoes-1.png" alt="" /> -->
								<?= Html::img("@web/images/{$sale1->img}", ['alt' => $sale1->name]) ?>
								<div class="mask">
									<span>Узнать больше</span>
								</div>
							</a>
							<h4><?php echo $sale1->name ?></h4>
							<p><span class="item_price"><?php echo $sale1->price; ?>руб</span></p>
						<!--<p><a href="<?php echo Url::to(['cart/add', 'id' => $sale1->id]);?>" data-id="<?php echo $sale1->id;?>" class="item_add"><span class=" item_price"><?php echo $sale1->price; ?>руб</span></a></p>-->
						    <!--<p><a class="item_add" href="#"><i></i> <span class=" item_price"><?php echo $sale1->price ?>руб</span></a></p>-->	
									
								</div>
								<?= Html::img("@web/images/sale.png", ['alt' => 'Распродажа', 'class' => 'sale']) ?>
							</div>
							<?php endforeach; ?>							
							<div class="clearfix"> </div>
						</div>
					</div>
					<?php endif;?>
				</div>
				<div class="col-md-3 single-right">
					<h3>Реклама</h3>
					<ul class="product-categories">
                        <?= Html::img("@web/images/shipping.jpg", ['alt' => 'Shape Shop']); ?>
					</ul>
					<h3>Реклама</h3>
					<ul class="product-categories">
                        <?= Html::img("@web/images/shipping.jpg", ['alt' => 'Shape Shop']); ?>
					</ul>
					<h3>Реклама</h3>
					<ul class="product-categories">
                        <?= Html::img("@web/images/shipping.jpg", ['alt' => 'Shape Shop']); ?>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<?php endif;?>
	<!--end-single<?php echo Url::to(['cart/add', 'id' => $product->id]);?>-->