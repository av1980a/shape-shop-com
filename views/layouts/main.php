<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\MenuWidget;
use yii\base\Widget;
use yii\helpers\Url;
use yii\bootstrap\Modal;

mihaildev\elfinder\Assets::noConflict($this);
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<html>
<head>
	 <?php $this->head() ?>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
   
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,300,400,500,700,800,900,100italic,300italic,400italic,500italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<!--//fonts-->
<!-- start Dropdown Menu Bootstrap -->
<script>$(document).ready(function(){$(".memenu").memenu();});</script>				
</head>
<body> 
<?php $this->beginBody() ?>
	<!--top-header-->
	<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-header-left">
				<div class="search-bar">
				<form method="get" action="<?php echo Url::to(['category/search'])?>">
				<input type="text" placeholder="Поиск" name="searh">
				</form>
				<input type="submit" value="">					
				</div>
			</div>
			<div class="col-md-4 top-header-middle">
				<a href="<?php echo Url::home();?>"><?= Html::img("@web/images/logo-4.png", ['alt' => 'Shape Shop']); ?></a>
			</div>
			<div class="col-md-4 top-header-right">
				<div class="cart box_1">
						<h3><div class="total"><a href="#" onclick="return getCart()"><span class="total">Корзина</span></div>
                         <!--<span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> предм.)</div> -->
                         
							 <?= Html::img("@web/images/cart-1.png", ['alt' => 'Shape Shop']); ?> 
						</a></h3>
						<!-- <p><a href="javascript:;" class="simpleCart_empty">Очистить</a></p> -->
						<div class="clearfix"> </div>						
					</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--top-header-->
	<!--bottom-header-->
		<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<ul class="memenu skyblue">
				 <li class="grid"><a href="<?php echo Url::home();?>">Главная</a></li>
				 <?php echo MenuWidget::widget() ?>   
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!--bottom-header-->
    <?php echo $content; ?>
	<!--start-footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-top">
				<div class="col-md-3 footer-left">
					<h3>О компании</h3>
					<ul>
						<li><a href="#">Кто мы</a></li>
						<li><a href="#">Контакты</a></li>
						<li><a href="#">СМИ о нас</a></li>
						<li><a href="#">Команда</a></li>
						<li><a href="#">Вакансии</a></li>					 
					</ul>
				</div>
				<div class="col-md-3 footer-left">
					<h3>Аккаунт</h3>
					<ul>
						<li><a href="account.html">Мой аккаунт</a></li>
						<li><a href="#">Профиль</a></li>
						<li><a href="#">Контакты</a></li>
						<li><a href="#">Трэкинг</a></li>
						<li><a href="#">История покупок</a></li>					 					 
					</ul>
				</div>
				<div class="col-md-3 footer-left">
					<h3>Сервисы</h3>
					<ul>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">Доставка</a></li>
						<li><a href="#">Возврат</a></li>
						<li><a href="#">Оплата</a></li>
						<li><a href="#">Гид покупателя</a></li>					 
					</ul>
				</div>
				<div class="col-md-3 footer-left">
					<h3>Категории</h3>
					<ul>
						<li><a href="#">Тест категории 1</a></li>
						<li><a href="#">Тест категории 6</a></li>
						<li><a href="#">Тест категории 11</a></li>
						<li><a href="#">Тест категории 20</a></li>
						<li><a href="#">Тест категории 30</a></li>				 
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-footer-->
	<!--end-footer-text-->
	<div class="footer-text">
		<div class="container">
			<div class="footer-main">
				<p class="footer-class">© 2018 Yii2 ecommerce-starter by A.Aramenko </p>
			</div>
		</div>
		</div>
	<!--end-footer-text-->	
	 <?php Modal::begin([
	    'header' => '<h2>Корзина</h2>',
	    'id' => 'cart',
	     'size' => '',
	    'footer' => ' <button type="button" class="btn btn-default" data-dismiss="modal">Продолжить покупки</button>
                      <a href="' .  yii\helpers\Url::to(['cart/view']) . '" class="btn btn-success">Оформить заказ</a>
	                  <button type="button" class="btn btn-danger" onclick="clearCart()" >Очистить корзину</button>',
	]);
	Modal::end();?> 

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>