<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!-- <script src="/js/cart.js"></script> -->
<!-- <script src="/js/jquery-1.11.0.min.js"></script> -->
<!-- <script type="text/javascript" src="/js/simpleCart.min.js"></script> -->
<!-- <link href="/css/memenu.css" rel="stylesheet" type="text/css" media="all" /> -->
<!-- <script type="text/javascript" src="/js/memenu.js"></script> -->
<!-- <script type="text/javascript" src="/js/move-top.js"></script> -->
<!-- <script type="text/javascript" src="/js/easing.js"></script> -->
<!-- <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->

<!-- <link href="/css/style.css" rel="stylesheet" type="text/css" media="all" /> -->
  <!-- <script defer src="/js/jquery.flexslider.js"></script> -->
<!-- <link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="screen" /> -->
<script>$(document).ready(function(){$(".memenu").memenu();});</script>	

<?php 
	//При переводе сайта на подгрузку картинок товара через админку (rico\yii2images) - раскомментить строки.
	//$mainImg = $product->getImage();
	//$gallery = $product->getImages();
	//$count = count($gallery);
	//$i = 0;
	?>

	<?php 
	//При переводе сайта на подгрузку картинок товара через админку (rico\yii2images) - все ссылки на картинки переписать следующим образом:
	//echo Html::img($mainImg->getUrl(), ['alt' => $product->name]) - для главной картинки
	//*****
	?>
	
	<?php 
	//Для галереи следующие 10 строк:
	//foreach ($gallery as $img):
	//if($i % 4 == 0): 
	?>
	<div class = "item <?php //if ($i == 0) echo 'active'?>">
	 <?php //endif;?>
	 <?php // Здесь вывод src через echo Html::img($mainImg->getUrl(), ['alt' => $product->name]).?>
	 <?php //if ($i % 4 == 0): ?>	 
	 <?php //endif;?>
	 <?php //endforeach;?>
	   </div>

		<!--banner-starts-->
		<?php //echo print_r($sale, true);?>
		<div class="bnr" id="home">
		<div  id="top" class="callbacks_container">
			<ul class="rslides" id="slider4">
			    <li>
					<div class="banner-1"></div>
				</li>
				<li>
					<div class="banner-2"></div>
				</li>
				<li>
					<div class="banner-3"></div>
				</li>
			</ul>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!--banner-ends--> 
	<!--Slider-Starts-Here-->
				<!-- <script src="js/responsiveslides.min.js"></script> -->
			 <script>
			    // You can also use "$(window).load(function() {"
			    $(function () {
			      // Slideshow 4
			      $("#slider4").responsiveSlides({
			        auto: true,
			        pager: true,
			        nav: false,
			        speed: 500,
			        namespace: "callbacks",
			        before: function () {
			          $('.events').append("<li>before event fired.</li>");
			        },
			        after: function () {
			          $('.events').append("<li>after event fired.</li>");
			        }
			      });
			
			    });
			  </script>
			<!--End-slider-script-->
	<!--start-banner-bottom--> 	
	<div class="banner-bottom">
		<div class="container">
			<div class="banner-bottom-top">
			<?php if( !empty('$new') ): ?>
			<?php foreach ($new as $new1): ?>
				<div class="col-md-6 banner-bottom-left">
					<div class="bnr-one">
						<div class="bnr-left">
							<h1><a href="<?php echo Url::to(['product/view', 'id' => $new1->id])?>"><?php echo $new1->name ?></a></h1>
							<p><?php echo $new1->description ?></p>
							<div class="b-btn"> 
								<a href="<?php echo Url::to(['product/view', 'id' => $new1->id])?>">Новинка!</a>
							</div>
						</div>
						<div class="bnr-right"> 
							<a href="<?php echo Url::to(['product/view', 'id' => $new1->id])?>"><?= Html::img("@web/images/light-{$new1->img}", ['alt' => $new1->name]) ?></a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif;?>
				<?php if( !empty('$hit') ): ?>
			    <?php foreach ($hit as $hit1): ?>
				<div class="col-md-6 banner-bottom-right">
					<div class="bnr-two">
						<div class="bnr-left">
							<h2><a href="<?php echo Url::to(['product/view', 'id' => $hit1->id])?>"><?php echo $hit1->name ?></a></h2>
							<p><?php echo $hit1->description ?></p>
							<div class="b-btn"> 
								<a href="<?php echo Url::to(['product/view', 'id' => $hit1->id])?>">Хит продаж!</a>
							</div>
						</div>
						<div class="bnr-right"> 
							<a href="<?php echo Url::to(['product/view', 'id' => $hit1->id])?>"><?= Html::img("@web/images/light-{$hit1->img}", ['alt' => $hit1->name]) ?></a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif;?>
				<div class="clearfix"> </div>				
			</div>
		</div>
	</div>	
	<!--end-banner-bottom--> 
	<!--start-shoes--> 
	<?php if( !empty('$sale') ): ?>
		<div class="shoes"> 
		<div class="container"> 
		<h7>Товары месяца</h7>
			<div class="product-one">
			<?php foreach ($sale as $sale1): ?>
				<div class="col-md-3 product-left"> 
					<div class="p-one simpleCart_shelfItem">												
							<a href="<?php echo Url::to(['product/view', 'id' => $sale1->id])?>">
								<!-- <img src="/images/shoes-1.png" alt="" /> -->
								<?= Html::img("@web/images/{$sale1->img}", ['alt' => $sale1->name]) ?>
								<div class="mask">
									<span>Узнать больше</span>
								</div>
							</a>
						<h4><?php echo $sale1->name ?></h4>
						<p><span class="item_price"><?php echo $sale1->price; ?>руб</span></p>
						<!--<p><a href="<?php echo Url::to(['cart/add', 'id' => $sale1->id]);?>" data-id="<?php echo $sale1->id;?>" class="item_add"><span class=" item_price"><?php echo $sale1->price; ?>руб</span></a></p>-->					
					</div>
					<?= Html::img("@web/images/sale.png", ['alt' => 'Распродажа', 'class' => 'sale']) ?>
				</div>
				<?php endforeach; ?>
				<div class="clearfix"> </div>
			</div>			
		</div>
	</div>
	<?php endif;?>
	<!--end-shoes-->
	<!--start-abt-shoe-->	
	<div class="abt-shoe">
		<div class="container"> 
			<div class="abt-shoe-main">
				<div class="col-md-4 abt-shoe-left">
					<div class="abt-one">
						<a href="#"><?= Html::img("@web/images/abt-1.jpg", ['alt' => 'Shape Shop']); ?></a>
						<h4><a href="single.html">Тест баннера 1</a></h4>
						<p>Тестовое описание баннера 1. </p>
					</div>
				</div>
				<div class="col-md-4 abt-shoe-left">
					<div class="abt-one">
						<a href="#"><?= Html::img("@web/images/abt-2.jpg", ['alt' => 'Shape Shop']); ?></a>
						<h4><a href="single.html">Тест баннера 2</a></h4>
						<p>Тестовое описание баннера 2. </p>
					</div>
				</div>
				<div class="col-md-4 abt-shoe-left">
					<div class="abt-one">
						<a href="#"><?= Html::img("@web/images/abt-3.jpg", ['alt' => 'Shape Shop']); ?></a>
						<h4><a href="single.html">Тест баннера 3</a></h4>
						<p>Тестовое описание баннера 3. </p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-abt-shoe-->
