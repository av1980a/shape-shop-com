<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

//$this->title = 'Shape Shop - одежда для активных';
?>

<script>$(document).ready(function(){$(".memenu").memenu();});</script>	

<?php 
	//При переводе сайта на подгрузку картинок товара через админку (rico\yii2images) - раскомментить строки.
	//$mainImg = $product->getImage();
	//$gallery = $product->getImages();
	//$count = count($gallery);
	//$i = 0;
	?>

	<?php 
	//При переводе сайта на подгрузку картинок товара через админку (rico\yii2images) - все ссылки на картинки переписать следующим образом:
	//echo Html::img($mainImg->getUrl(), ['alt' => $product->name]) - для главной картинки
	//*****
	?>
	
	<?php 
	//Для галереи следующие 10 строк:
	//foreach ($gallery as $img):
	//if($i % 4 == 0): 
	?>
	<div class = "item <?php //if ($i == 0) echo 'active'?>">
	 <?php //endif;?>
	 <?php // Здесь вывод src через echo Html::img($mainImg->getUrl(), ['alt' => $product->name]).?>
	 <?php //if ($i % 4 == 0): ?>	 
	 <?php //endif;?>
	 <?php //endforeach;?>
	   </div>
	   

	<!--start-shoes--> 
	<?php //echo var_dump($products) ?>
	<?php if( count($products) > 0): ?>
		<div class="shoes"> 
		<div class="container"> 
		<h7><?php echo 'Поиск по запросу:  ' . Html::encode($searh);?></h7>
			<div class="product-one">
			<?php foreach ($products as $product): ?>
				<div class="col-md-3 product-left"> 
					<div class="p-one simpleCart_shelfItem">												
							<a href="<?php echo Url::to(['product/view', 'id' => $product->id])?>">
								<!-- <img src="/images/shoes-1.png" alt="" /> -->
								<?= Html::img("@web/images/{$product->img}", ['alt' => $product->name]) ?>
								<div class="mask">
									<span>Подробнее</span>
								</div>
							</a>
						<h4><?php echo $product->name ?></h4>
						<p><span class=" item_price"><?php echo $product->price; ?>руб</span></p>
						<!--<p><a href="<?php echo Url::to(['cart/add', 'id' => $product->id]);?>" data-id="<?php echo $product->id;?>" class="item_add"><span class=" item_price"><?php echo $product->price; ?>руб</span></a></p>-->
						<!--<p><a class="item_add" href="#"><i></i> <span class=" item_price"><?php echo $product->price ?>руб</span></a></p>-->					
					</div>
				</div>
				<?php endforeach; ?>
				<div class="clearfix"> </div>				
			</div>	
			<?php echo LinkPager::widget(['pagination' => $pages]); ?>		
		</div>
	</div>
					<?php else:?>
	                       <h7>Товары по данному запросу отсутствуют.</h7>
	<?php endif;?>
	<!--end-shoes-->
	