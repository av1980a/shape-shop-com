<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\MenuWidget;
use yii\base\Widget;
use yii\helpers\Url;
use yii\bootstrap\Modal;

mihaildev\elfinder\Assets::noConflict($this);
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<html>
<head>
	 <?php $this->head() ?>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Панель Администратора</title>
   
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,300,400,500,700,800,900,100italic,300italic,400italic,500italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<!--//fonts-->
<!-- start Dropdown Menu Bootstrap -->
				
</head>
<body> 
<?php $this->beginBody() ?>
	<!--top-header-->
	<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-header-left">
				
			</div>
			<div class="col-md-4 top-header-middle">
				<a href="<?php echo Url::home();?>"><?= Html::img("@web/images/logo-4.png", ['alt' => 'Shape Shop']); ?></a>
			</div>
			<div class="col-md-4 top-header-right">
				<?php if (!Yii::$app->user->isGuest) : ;?>
				<div class="cart box_1">
				<h3><div class="total"><a href="<?php echo Url::to(['/site/logout']); ?>"><span class="total"><?php echo Yii::$app->user->identity['username']?> // Выход</span></div></a></h3>
				</div>
				<?php endif;?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--top-header-->
	<!--bottom-header-->
		<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<ul class="memenu skyblue">
				<li class="grid"><a href="<?php echo Url::to(['/admin'])?>">Заказы</a></li>
				<li class="grid"><a href="<?php echo Url::to(['category/index'])?>">Категории</a></li>
				<li class="grid"><a href="<?php echo Url::to(['product/index'])?>">Продукты</a></li>				
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>	
	<!--bottom-header-->
	<div class="container">
<?php if (Yii::$app->session->hasFlash('success')):?>
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
<?php echo Yii::$app->session->getFlash('success');?>
</div>
<?php endif; ?>
</div>
    <div class="container">
    <?php echo $content; ?>
    </div>
	<!--start-footer-->
	
	<!--end-footer-->
	<!--end-footer-text-->
	<div class="container">
	<div class="footer-text">
		<div class="container">
			<div class="footer-main">
				<p class="footer-class">© 2018 Yii2 ecommerce-starter by A.Aramenko </p>
			</div>
		</div>
		</div>
		</div>
	<!--end-footer-text-->		 

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>