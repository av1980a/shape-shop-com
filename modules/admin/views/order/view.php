<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1>Заказ N<?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите это удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'updated_at',
            'qty',
            'sum',
            [
                'attribute' => 'status',
                'value' => function ($data){
                return !$data->status ? '<span class="text-danger">Активен</span>' : '<span class="text-success">Завершён</span>';
                },
                'format' => 'html',
                ],
            //'status',
            'name',
            'email:email',
            'phone',
            'address',
        ],
    ]) ?>
    
    <?php //echo var_dump($model->orderItems);?>
    
    <?php $items = $model->orderItems;?>
    
   <div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>				
				<th>Наименование</th>
				<th>Кол-во</th>
				<th>Цена</th>
				<th>Сумма</th>				
			</tr>
		</thead>
		<tbody>
<?php foreach ($items as $item):?>
            <tr>				
				<td><a href="<?php echo Url::to(['/product/view', 'id' => $item['product_id']])?>"><?php  echo $item['name']?></a></td>
				<td><?php  echo $item['qty_item']?></td>
				<td><?php  echo $item['price']?></td>
				<td><?php  echo $item['sum_item']?></td>				
			</tr>
<?php endforeach;?>		
		</tbody>
	</table>
</div>


</div>
