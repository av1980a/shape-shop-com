<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать продукт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'category_id',
            [
                'attribute' => 'category_id',
                'value' => function ($data){
                if (isset($data->category->name)){
                    return $data->category->name ? $data->category->name : 'Не задано';
                }
                },
                'format' => 'html',
                ],
            'name',
            //'content:ntext',
            'price',
            //'keywords',
            //'description',
            //'img',
                [
                    'attribute' => 'sale',
                    'value' => function ($data){
                    return !$data->sale ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>';
                    },
                    'format' => 'html',
                    ],
            //'sale',
                    [
                        'attribute' => 'new',
                        'value' => function ($data){
                        return !$data->new ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>';
                        },
                        'format' => 'html',
                        ],
            //'new',
                        [
                            'attribute' => 'hit',
                            'value' => function ($data){
                            return !$data->hit ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>';
                            },
                            'format' => 'html',
                            ],
            //'hit',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
