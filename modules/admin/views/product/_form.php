<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Product;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;




/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
       
    <?php echo $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Product::find()->all(), 'id', 'name'))?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'content')->widget(CKEditor::className(), ['editorOptions' => ElFinder::ckeditorOptions('elfinder',['preset' => 'full', 'inline' => false])]);?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->fileInput() ?>
    
    <?= $form->field($model, 'gallery')->fileInput(['multiple' => true, 'accept' => 'upload/store/*']) ?>

    <?= $form->field($model, 'sale')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]) ?>

    <?= $form->field($model, 'new')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]) ?>

    <?= $form->field($model, 'hit')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
