<?php
namespace app\controllers;
use app\controllers\AppController;
use app\models\Category;
use app\models\Product;
use app\models\Cart;
use app\models\Order;
use app\models\OrderItems;
use Yii;
use yii\data\Pagination;
use yii\web\HttpException;

class CartController extends  AppController {
    
    public function actionAdd() {
        $id = Yii::$app->request->get('id');
        $qty = (int)Yii::$app->request->get('qty');
        $qty = !$qty ? 1 : $qty;
        //echo var_dump($id);
        $product = Product::findOne($id);
        if (empty($product)) return false;
        //echo var_dump($product);
        //echo print_r($product);
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->addToCart($product, $qty);
        //echo print_r($session['cart']);
        //echo ' - qty ' .  var_dump($session['cart.qty']);
        //echo ' - sum ' .  var_dump($session['cart.sum']);
        //$session->destroy();
        $this->layout = false;        
        //return $this->renderPartial('cart-modal', compact('session'), false, true);
        //return $this->renderAjax('cart-modal', compact('session'));
        //return $this->render('cart-modal', compact('session'));  
        //return $this->render('/category/index', compact('session'));
        //exit();
        //return false;
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionClear() {
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.qty');
        $session->remove('cart.sum');
        //echo print_r($session['cart']);
        //echo ' - qty ' .  var_dump($session['cart.qty']);
        //echo ' - sum ' .  var_dump($session['cart.sum']);
        //$session->destroy();
        $this->layout = false;
        //return $this->renderPartial('cart-modal', compact('session'), false, true);
        //return $this->renderAjax('cart-modal', compact('session'));
        return $this->render('cart-modal', compact('session'));
    }
    
    public function actionDelitem() {
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalc($id);
        //echo print_r($session['cart']);
        //echo ' - qty ' .  var_dump($session['cart.qty']);
        //echo ' - sum ' .  var_dump($session['cart.sum']);
        //$session->destroy();
        //$this->layout = false;
        //return $this->renderPartial('cart-modal', compact('session'), false, true);
        //return $this->renderAjax('cart-modal', compact('session'));
        //return $this->render('cart-modal', compact('session'));
        return $this->redirect(Yii::$app->request->referrer);
        //return $this->actionView();
    }
    
    public function actionShow(){
        $session = Yii::$app->session;
        $session->open();
        $this->layout = false;
        return $this->render('cart-modal', compact('session'));
    }
    
    public function actionView() {
        $session = Yii::$app->session;
        $session->open();
        $order = new Order();
        if ($order->load(\Yii::$app->request->post())) {
            $order->qty = $session['cart.qty'];
            $order->sum = $session['cart.sum'];
            //echo var_dump(\Yii::$app->request->post());
            if ($order->save()) {
                $this->saveOrderItems($session['cart'], $order->id);
                \Yii::$app->session->setFlash('success', 'Ваш заказ принят.');                
                Yii::$app->mailer->compose('order', ['session' => $session])
                ->setFrom(['usernaim@mail.ru' => 'shape-shop.com']) //исходящий адрес, !соотв. настройкам мэйлера в web.php
                ->setTo($order->email)
                ->setSubject('Заказ с сайта...')                
                ->send();
                Yii::$app->mailer->compose('order', ['session' => $session])
                ->setFrom(['usernaim@mail.ru' => 'shape-shop.com']) //исходящий адрес, соотв. настройкам мэйлера в web.php
                ->setTo(\Yii::$app->params['adminEmail'])//адрес доставки, соотв. настройкам в params.php
                ->setSubject('Заказ с сайта...')
                ->send();
                $session->remove('cart');
                $session->remove('cart.qty');
                $session->remove('cart.sum');
                return $this->refresh();
            }
            else  {
                \Yii::$app->session->setFlash('error', 'Ошибка оформления.');
            }
        }
        $this->setMeta('Shape Shop - одежда для активных', 'Тест ключевиков корзины', 'Тест описания корзины');
        return $this->render('view', compact('session', 'order'));
    }
    
    protected function saveOrderItems($items, $order_id) {
        foreach ($items as $id => $item) {
            $order_items = new OrderItems();
            $order_items->order_id = $order_id;
            $order_items->product_id = $id;
            $order_items->name = $item['name'];
            $order_items->price = $item['price'];
            $order_items->qty_item = $item['qty'];
            $order_items->sum_item = $item['qty'] * $item['price'];
            $order_items->save();
        }
    }
}

