<?php
namespace app\controllers;
use app\controllers\AppController;
use app\models\Category;
use app\models\Product;
use Yii;
use yii\web\HttpException;

class ProductController extends  AppController {
    public function actionView($id) {
        $sale = Product::find()->where(['sale' => '1'])->limit(6)->all();
        //$id = Yii::$app->request->get('id');
        //$product = Product::findOne($id);
        $product = Product::find()->with('category')->where(['id' => $id])->limit(1)->one();
        if (empty($product))
            throw new HttpException(404, 'Такой товар не существует');
        if( count($product) > 0): ;
        $this->setMeta('Shape Shop |' . $product->name, $product->keywords, $product->description);
        else:
        $this->setMeta('Shape Shop - одежда для активных', 'Тест общих ключевиков', 'Тест общего описания');
        endif;
        //$this->setMeta('Shape Shop |' . $product->name, $product->keywords, $product->description);
        return $this->render('view', compact('sale', 'product', 'category'));
}
}
