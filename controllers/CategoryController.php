<?php
namespace app\controllers;
use app\controllers\AppController;
use app\models\Category;
use app\models\Product;
use Yii;
use yii\data\Pagination;
use yii\web\HttpException;

class CategoryController extends  AppController {
    public function actionIndex() {
        $sale = Product::find()->where(['sale' => '1'])->limit(8)->all();
        $new = Product::find()->where(['new' => '1'])->limit(1)->all();
        $hit = Product::find()->where(['hit' => '1'])->limit(1)->all();
        //echo print_r($sale, true);
        $this->setMeta('Shape Shop - одежда для активных', 'Тест общих ключевиков', 'Тест общего описания');
        return $this->render('index', compact('sale', 'new', 'hit'));
    }
    
    public function actionView($id) {
        //$id = Yii::$app->request->get('id');
        $category = Category::findOne($id);
        if (empty($category))
            throw new HttpException(404, 'Такой категории не существует');
        //echo var_dump($id);
        //$products = Product::find()->where(['category_id' => $id])->limit(12000)->all();
        $query = Product::find()->where(['category_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 8, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        
        //echo var_dump($products);
        if( count($category) > 0): ;
        $this->setMeta('Shape Shop |' . $category->name, $category->keywords, $category->description);
        else:
        $this->setMeta('Shape Shop - одежда для активных', 'Тест общих ключевиков', 'Тест общего описания');
        endif;
        return $this->render('view', compact('products', 'category', 'pages'));
    }
    
    public function actionSearch() {
        $searh = trim(Yii::$app->request->get('searh'));
        $query = Product::find()->where(['like', 'name', $searh]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 8, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        //echo var_dump($products);
        $this->setMeta('Shape Shop - одежда для активных', 'Тест общих ключевиков', 'Тест общего описания');
        return $this->render('search', compact('products', 'category', 'searh', 'pages'));
    }
}
